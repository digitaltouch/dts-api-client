//import DtsApiAnalytics from './analytics';
import DtsApiClient from './client';
import DtsApiLogger from './logger';
import DtsApiManager from './manager';
import DtsApiProfile from './profile';
import DtsApiProvider from './provider';

import signals from 'signals';

class Frontend {
  constructor(config) {
    this._config = config || {};

    this._isAuthenticated = false;
    this.isAuthenticatedChanged = new signals.Signal();

    this._modules = [];

    this._logger = new DtsApiLogger(this.config);
    this._modules.push(this._logger);

    const options = {
      proxy: () => this._ensureApiAccess(),
      awsproxy: () => this._ensureCloudAccess(),
      logger: this._logger
    };

    if (this.config.cognito) {
      this._provider = new DtsApiProvider(this.config, options);
      this._profile = new DtsApiProfile(this.config, options);
      //this._modules.push(this._profile);
    }

    if (this.config.analytics) {
      // this._analytics = new DtsApiAnalytics(this.config, options);
      // this._modules.push(this._analytics);
    }

    this._client = new DtsApiClient(this.config, options);
    this._client.sessionError.add(err => {
      this._logger.debug({ error: err }, 'Session error.');
      (this.provider ? this.provider.getUserToken() : Promise.resolve())
      .catch(err => {
        this._logger.warn({ error: err }, 'AWS access is not available.');
      });
    });

    this._manager = new DtsApiManager(this.config, options);
    this._manager.tokenExpired.add(token => {
      this._logger.debug({ token: token }, 'API token has expired.');
      this._refreshAccess().catch(err => {
        this._logger.warn({ error: err }, 'API access is not available.');
      });
    });
  }

  get config() {
    return this._config;
  }

  get analytics() {
    return this._analytics;
  }

  get client() {
    return this._client;
  }

  get logger() {
    return this._logger;
  }

  get manager() {
    return this._manager;
  }

  get profile() {
    return this._profile;
  }

  get provider() {
    return this._provider;
  }

  get currentAccess() {
    const application = this.config.application,
          token = this.manager.getToken(application);

    if (!token || !token.access_token || (!token.cognito_token && !token.refresh_token)) {
      return null;
    }

    return this.manager.getTokenMetadata(token);
  }

  get currentUser() {
    if (!this.provider) {
      return null;
    }

    return this.provider.user;
  }

  get isAuthenticated() {
    if (!this.provider) {
      return Boolean(this.currentAccess);
    }

    return Boolean(this.currentUser) && Boolean(this.currentAccess);
  }

  initialize() {
    return Promise.resolve()
    .then(() => {
      if (this.client.socket) {
        return this._startPing();
      }
    })
    .then(() => !this.provider || this.provider.initialize())
    .then(() => !this._profile || this._profile.initialize())
    .then(() => {
      // if (this.provider && !this.currentUser) {
      //   return this.signOut();
      // }

      return this.manager.initialize().then(() => {
        if (!this.currentAccess) {
          return this.signOut();
        }

        const application = this.config.application,
              token = this.manager.getToken(application);

        if (token) {
          return this.client.authorize(application, token);
        }
      })
      .catch(err => {
        this._logger.error({ error: err }, 'Unable to restore the API session');
        return Promise.reject(err);
      });
    })
    .then(() => {
      return Promise.all(this._modules.map(x => x.initialize()))
      .catch(err => {
        this._logger.error({ error: err }, 'Unable to initialize modules.');
        return Promise.reject(err);
      });
    })
    .then(() => {
      if (!this.isAuthenticated && this.provider && this.provider.identity) {
        const identity = this.provider.identity;

        if (identity.pending) {
          return;
        }

        this._logger.debug({ identity: identity.token, password: identity.password }, 'Trying to restore the identity...');

        return this.provider.signIn({
          username: identity.token,
          password: identity.password
        })
        .then(() => {
          return this._refreshAccess();
        });
      }
    })
    .catch(err => {
      if (err.code === 'UserNotConfirmedException') {
        return;
      }

      if (err.code === 'UserNotFoundException') {
        return this.provider.resetIdentity()
        .then(() => this.signOut())
        .then(() => Promise.reject(err));
      }

      return Promise.reject(err);
    })
    .then(() => this._updateAuthStatus())
    .catch(err => {
      this._logger.warn({ error: err }, 'Client initialize error.');
      //this.signOut();

      return Promise.reject(err);
    });
  }

  assumeIdentity(request) {
    if (!this.provider) {
      return Promise.reject(this._logger.error('Device identity is not supported.'));
    }

    return this.provider.assumeIdentity(request);
  }

  authenticate(token) {
    const application = this.config.application;

    return Promise.all([
      this.client.authorize(application, token),
      this.manager.saveToken(application, token)
    ])
    .then(() => this.logger.debug('API token has been received.'))
    .then(() => this._updateAuthStatus());
  }

  signIn(request) {
    if (!this.provider) {
      const application = this.config.application;

      return this.client.api.request('oauth2', 'grantToken', {
        grant_type: 'password',
        client_id: application.client_id,
        username: request.username,
        password: request.password
      }, true)
      .then(token => {
        if (!token || !token.access_token) {
          return Promise.reject(this._logger.error({ result: token }, 'Invalid token received.'));
        }

        return this.authenticate(token);
      })
      .catch(err => {
        return this.signOut()
          .then(() => this._updateAuthStatus())
          .then(() => Promise.reject(err));
      });
    }

    return this.provider.signIn(request)
    .then(() => this._refreshAccess())
    .catch(err => {
      this._logger.warn({ error: err }, 'Sign in error.');
      return Promise.reject(err);
      // return this.signOut()
      // .then(() => {
      //   this._logger.warn({ error: err }, 'Sign in error.');
      //   return Promise.reject(err);
      // });
    });
  }

  getDeviceStatus() {
    if (!this.provider) {
      return Promise.reject(this._logger.error('Device status is not supported.'));
    }

    const identity = this.provider.identity;

    if (!identity || !identity.token) {
      return Promise.reject(this._logger.error('No active identity.'));
    }

    return this.client.api.request('device', 'deviceRead', {
      token: identity.token
    }, true)
    .catch(err => {
      if (err.status === 404) {
        return this.provider.resetIdentity()
        .then(() => Promise.reject(err));
      }

      return Promise.reject(err);
    });
  }

  updateSession(data) {
    data = data || {};

    const application = this.config.application;
    const token = this.manager.getToken(application);

    if (!token) {
      return Promise.reject(this._logger.error('Missing token.'));
    }

    const metadata = this.manager.getTokenMetadata(token),
          locationId = data.location !== undefined ? data.location : metadata.location,
          seatId = data.seat !== undefined ? data.seat : metadata.seat,
          employeeId = data.employee !== undefined ? data.employee : metadata.employee;

    if (!this.provider) {
      if (!token.refresh_token) {
        return Promise.reject(this._logger.error('Missing refresh token.'));
      }

      return this.client.api.request('oauth2', 'grantToken', {
        grant_type: 'refresh_token',
        client_id: application.client_id,
        refresh_token: token.refresh_token,
        location_id: locationId,
        seat_id: seatId,
        employee_id: employeeId
      }, true)
      .then(token => {
        if (!token || !token.access_token) {
          return Promise.reject(this._logger.error({ result: token }, 'Unable to update the session.'));
        }

        return this.authenticate(token);
      });
    }
    else {
      return this.provider.getUserToken()
      .then(cognito => {
        if (!cognito) {
          return Promise.reject(this._logger.error('Missing user token.'));
        }

        return this.client.api.request('oauth2', 'grantToken', {
          grant_type: 'cognito_credentials',
          client_id: application.client_id,
          access_token: cognito,
          location_id: locationId,
          seat_id: seatId,
          employee_id: employeeId
        }, true)
        .then(token => {
          if (!token || !token.access_token) {
            return Promise.reject(this._logger.error({ result: token }, 'Unable to update the session.'));
          }

          return this.authenticate(token);
        });
      });
    }
  }

  signOut() {
    return (this.provider ? this.provider.signOut() : Promise.resolve())
    .then(() => {
      const application = this.config.application;

      return Promise.all([
        this.manager.removeToken(application),
        this.client.deauthorize(application)
      ]);
    })
    .then(() => this._updateAuthStatus());
  }

  enablePushNotifications(push, options) {
    options = options || {};

    push.on('registration', data => {
      this._logger.debug({ data: data }, 'Push notifications enabled.');

      const register = () => {
        if (!this.isAuthenticated) {
          return;
        }

        // this.profile.registerDevice(data.registrationId)
        // .catch(err => this._logger.error({ error: err }, 'Device registration error.'));

        this.client.api.request('notification', 'notificationCreate', { body: {
          registration: data.registrationId,
          platform: options.platform,
          business: options.business
        } })
        .catch(err => this._logger.error({ error: err }, 'Push notifications error.'));
      };

      if (!this.isAuthenticated) {
        this.isAuthenticatedChanged.addOnce(() => register());
        return;
      }

      register();
    });

    push.on('notification', data => {
      this._logger.debug({ data: data }, 'Push notification received.');

      if (!this.isAuthenticated) {
        return;
      }
    });
  }

  _refreshAccess() {
    if (this._refreshAccessPromise) {
      return this._refreshAccessPromise;
    }

    const application = this.config.application;

    let request;

    if (!this.provider) {
      const token = this.manager.getToken(application);

      if (!token) {
        return Promise.reject(this.logger.error('Missing API access.'));
      }

      const metadata = this.manager.getTokenMetadata(token),
            locationId = metadata.location,
            seatId = metadata.seat,
            employeeId = metadata.employee;

      if (!token.refresh_token) {
        return Promise.reject(this.logger.error('Missing refresh token.'));
      }

      request = this.client.api.request('oauth2', 'grantToken', {
        grant_type: 'refresh_token',
        client_id: application.client_id,
        refresh_token: token.refresh_token,
        location_id: locationId,
        seat_id: seatId,
        employee_id: employeeId
      }, true);
    }

    this._refreshAccessPromise = (this.provider ? this.provider.getUserToken() : Promise.resolve())
    .then(cognito => {
      if (this.provider && !cognito) {
        return this.provider.getUserToken(true);
      }

      return cognito;
    })
    .then(cognito => {
      if (request) {
        return request;
      }

      if (this.provider && !cognito) {
        return Promise.reject(new Error('Missing user session.'));
      }

      let locationId, seatId, employeeId;

      const token = this.manager.getToken(application);

      if (token) {
        const metadata = this.manager.getTokenMetadata(token);
        locationId = metadata.location;
        seatId = metadata.seat;
        employeeId = metadata.employee;
      }

      return this.client.api.request('oauth2', 'grantToken', {
        grant_type: 'cognito_credentials',
        client_id: application.client_id,
        access_token: cognito,
        location_id: locationId,
        seat_id: seatId,
        employee_id: employeeId
      }, true)
      .catch(err => {
        if (err.error === 'invalid_request' || err.error === 'invalid_client') {
          if (!this.manager.isAccessValid(application)) {
            this._logger.error({ error: err }, 'Token refresh was declined.');
            return this.signOut().then(() => Promise.reject(err));
          }
        }

        return Promise.reject(err);
      });
    })
    .then(token => {
      if (!token.access_token) {
        return Promise.reject(this.logger.error({ result: token }, 'Invalid token received.'));
      }

      return this.authenticate(token);
    })
    .then(res => {
      this._refreshAccessPromise = null;
      return res;
    })
    .catch(err => {
      this._refreshAccessPromise = null;

      this._logger.warn({ error: err }, 'Unable to refresh a token.');

      if (this.isAuthenticated) {
        return;
      }

      return Promise.reject(err);
    });

    return this._refreshAccessPromise;
  }

  _ensureCloudAccess() {
    if (this._ensureCloudPromise) {
      return this._ensureCloudPromise;
    }

    this._ensureCloudPromise = new Promise((res, rej) => {
      const resolve = () => {
        this._ensureCloudPromise = null;
        res();
      };

      const reject = (e) => {
        this._ensureCloudPromise = null;
        rej(e);
      };

      if (!this.provider) {
        return resolve();
      }

      this.provider.getUserToken(true)
      .then(cognito => {
        if (!cognito) {
          return Promise.reject(new Error('No current token.'));
        }
      })
      .catch(err => {
        return this.provider.getUserToken();
      })
      .then(resolve)
      .catch(err => {
        reject(err);
      });
    });

    return this._ensureCloudPromise;
  }

  _ensureApiAccess() {
    if (this._ensurePromise) {
      return this._ensurePromise;
    }

    this._ensurePromise = new Promise((res, rej) => {
      const resolve = () => {
        this._ensurePromise = null;
        res();
      };

      const reject = (e) => {
        this._ensurePromise = null;
        rej(e);
      };

      const application = this.config.application,
            token = this.manager.getToken(application);

      if (!token || !token.access_token) {
        this.logger.error({ token: token }, 'Corrupted access token.');
        return this.signOut().then(resolve).catch(reject);
      }

      if (!this.manager.isTokenValid(token)) {
        this.logger.debug({ token: token }, 'Refreshing an expired access token.');
        return this._refreshAccess().then(resolve).catch(reject);
      }

      resolve();

      // if (!this.provider) {
      //   return resolve();
      // }
      //
      // this.provider.getUserToken(true)
      // .then(cognito => {
      //   if (!cognito) {
      //     this.logger.error({ token: token }, 'Unable to get current Cognito token.');
      //     return this.signOut();
      //   }
      //
      //   if (!this.manager.isTokenValid(cognito)) {
      //     this.logger.debug({ token: token }, 'Updating an expired access token.');
      //     return this._refreshAccess();
      //   }
      // })
      // .then(resolve).catch(reject);
    });

    return this._ensurePromise;
  }

  _updateAuthStatus() {
    const value = this.isAuthenticated;

    if (this.provider) {
      Promise.all(this._modules.map(x => x.authenticate(this.provider.user)))
      .catch(err => this._logger.error({ error: err }, 'Module authentication error.'));
    }

    if (this._isAuthenticated === value) {
      return this._isAuthenticated;
    }

    this._isAuthenticated = value;
    this.isAuthenticatedChanged.dispatch(value);

    return this._isAuthenticated;
  }

  _startPing() {
    let intervalID;

    const interval = 60 * 1000;
    const handler = () => {
      if (!this.client.socket.isConnected || !this.client.socket.isEnabled) {
        return;
      }

      const application = this.config.application;
      const token = this.manager.getToken(application);

      if (token) {
        const metadata = this.manager.getTokenMetadata(token);

        if (metadata && metadata.location) {
          this.client.socket.ping(metadata.location)
          .catch(err => this._logger.debug({ error: err }, 'Socket ping error.'));
        }
      }
    };

    this.client.socket.onPingError.add(() => {
      this._updateAuthStatus();

      const application = this.config.application;
      const token = this.manager.getToken(application);

      if (token) {
        this.client.authorize(application, token);
        this.client.socket.reconnect();
      }
    });

    this.client.socket.isAuthenticatedChanged.add(isAuthenticated => {
      if (isAuthenticated || this._isRefreshing) {
        return;
      }

      this._isRefreshing = true;
      setTimeout(() => this._isRefreshing = false, interval);

      if (intervalID) {
        clearInterval(intervalID);
      }

      intervalID = setInterval(handler, interval);

      this._refreshAccess()
      .catch(err => {})
      .then(() => {
        intervalID = setInterval(handler, interval);

        this.client.socket.reconnect();

        setTimeout(() => this._isRefreshing = false, 15 * 60 * 1000);
      });
    });

    intervalID = setInterval(handler, interval);
  }
}

export default Frontend;
