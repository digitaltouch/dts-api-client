import test from 'ava';

import Promise from 'bluebird';
import Watcher from './index';

const app = { security: 'oauth' },
      token = '';

test('manager/watcher: valid token', t => {
  const w = new Watcher(app);
  t.is(w.hasExpired, false);
  w.watch(token, 10);
  t.is(w.hasExpired, false);
  w.unwatch();
  t.is(w.hasExpired, false);
});

test('manager/watcher: expiring token', t => {
  const w = new Watcher(app);
  w.watch(token, 1);
  t.is(w.hasExpired, false);
  return Promise.delay(1000).then(() => t.is(w.hasExpired, true));
});

test('manager/watcher: expired token signal', t => {
  const w = new Watcher(app);
  w.watch(token, 1);
  return new Promise((resolve, reject) => {
    Promise.delay(1500).then(reject);
    w.tokenExpired.addOnce(() => {
      t.is(w.hasExpired, true);
      resolve();
    });
  });
});

test('manager/watcher: expired token', t => {
  const w = new Watcher(app);
  w.watch(token, 0);
  t.is(w.hasExpired, true);
});
