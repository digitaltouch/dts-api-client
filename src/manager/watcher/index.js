import signals from 'signals';

export default class Watcher {
  constructor(application) {
    this.application = application;

    this.tokenExpired = new signals.Signal();
  }

  watch(token, expires_in) {
    this.unwatch();

    this.token = token;

    if (expires_in > 0 && expires_in < 2147482) {
      this._timeout = setTimeout(() => this._onExpired(), expires_in * 1000);
    }
    else if (expires_in <= 0) {
      this._hasExpired = true;
      setTimeout(() => this._onExpired());
    }
  }

  unwatch() {
    this.token = undefined;
    this._hasExpired = false;

    if (this._timeout) {
      clearTimeout(this._timeout);
      this._timeout = undefined;
    }
  }

  get hasExpired() {
    return this._hasExpired || false;
  }

  _onExpired() {
    this._hasExpired = true;
    this.tokenExpired.dispatch({
      token: this.token,
      application: this.application
    });
  }
};
