import test from 'ava';

import jwt from 'jwt-simple';
import Promise from 'bluebird';
import Manager from './index';

const app = { security: 'oauth', client_id: 'test' },
      secret = '123',
      config = {
        application: app,
        storageType: 'memory'
      };

function createToken(payload, expires_in) {
  expires_in = expires_in !== undefined ? expires_in : 60;

  const now = Math.round(new Date().getTime() / 1000);

  const cognito = {
    iat: now,
    exp: now + expires_in
  };

  payload = Object.assign({}, payload);
  payload.iat = cognito.iat;
  payload.exp = cognito.exp;

  return {
    access_token: jwt.encode(payload, secret),
    cognito_token: jwt.encode(cognito, secret),
    expires_in: expires_in,
    token_type: 'bearer'
  };
}

test('manager: valid constructor', t => {
  const manager = new Manager(config);
  t.pass();
});

test('manager: invalid constructor', t => {
  try {
    new Manager();
    t.fail();
  }
  catch (err) {}

  try {
    new Manager({});
    t.fail();
  }
  catch (err) {}

  t.pass();
});

test('manager: initialize() regular', t => {
  t.plan(1);
  const manager = new Manager(config);
  return manager.initialize().then(metadata => t.is(Boolean(metadata), false));
});

test('manager: initialize() restore', t => {
  t.plan(2);
  const manager = new Manager(config);
  const token = { device: '123' };
  manager.saveToken(app, createToken(token));
  return manager.initialize().then(metadata => {
    t.is(Boolean(metadata), true);
    t.is(metadata.device, token.device);
  });
});

test('manager: initialize() regular', t => {
  t.plan(1);
  const manager = new Manager(config);
  return manager.initialize().then(metadata => t.is(Boolean(metadata), false));
});

test('manager: saveToken() valid arguments', t => {
  const manager = new Manager(config);
  manager.saveToken(app, createToken({}));
  manager.saveToken(app, createToken({ device: '123' }));
  t.pass();
});

test('manager: saveToken() invalid arguments', t => {
  try {
    let manager = new Manager(config);
    manager.saveToken();
    t.fail();
  }
  catch (err) {}

  try {
    let manager = new Manager(config);
    manager.saveToken({});
    t.fail();
  }
  catch (err) {}

  try {
    let manager = new Manager(config);
    manager.saveToken(app);
    t.fail();
  }
  catch (err) {}

  try {
    let manager = new Manager(config);
    let token = createToken({});
    delete token.access_token;
    manager.saveToken(app, token);
    t.fail();
  }
  catch (err) {}

  t.pass();
});

test('manager: getToken() valid arguments', t => {
  const manager = new Manager(config);
  const token = createToken({ device: '123' });
  manager.saveToken(app, token);
  const result = manager.getToken(app);

  t.is(Boolean(result), true);
  t.is(result.device, token.device);
});

test('manager: getToken() invalid arguments', t => {
  try {
    let manager = new Manager(config);
    manager.getToken();
    t.fail();
  }
  catch (err) {}

  try {
    let manager = new Manager(config);
    manager.getToken({});
    t.fail();
  }
  catch (err) {}

  t.pass();
});

test('manager: getTokenMetadata() valid arguments', t => {
  const manager = new Manager(config);
  const token = createToken({ device: '123' });
  const result = manager.getTokenMetadata(token);

  t.is(Boolean(result), true);
  t.is(result.device, '123');
});

test('manager: getTokenMetadata() invalid arguments', t => {
  const manager = new Manager(config);
  const token = createToken({ device: '123' });
  delete token.access_token;
  const result = manager.getTokenMetadata(token);
  t.is(Boolean(result), false);
  t.is(Boolean(manager.getTokenMetadata()), false);
  t.is(Boolean(manager.getTokenMetadata('')), false);
});

test('manager: isTokenValid() valid arguments', t => {
  const manager = new Manager(config);

  t.is(manager.isTokenValid(createToken({})), true);
  t.is(manager.isTokenValid(createToken({}, 0)), false);
  t.is(manager.isTokenValid(createToken({}, -10)), false);
});

test('manager: isAccessValid() valid arguments', t => {
  const manager = new Manager(config);
  t.is(manager.isAccessValid(app), false);
  manager.saveToken(app, createToken({ device: '123' }));
  t.is(manager.isAccessValid(app), true);
  manager.saveToken(app, createToken({ device: '123' }, 0));
  t.is(manager.isAccessValid(app), false);
  manager.removeToken(app);
  t.is(manager.isAccessValid(app), false);
});

test('manager: removeToken() invalid arguments', t => {
  try {
    let manager = new Manager(config);
    manager.removeToken();
    t.fail();
  }
  catch (err) {}

  try {
    let manager = new Manager(config);
    manager.removeToken({});
    t.fail();
  }
  catch (err) {}

  t.pass();
});

test('manager: expired token signal', t => {
  const manager = new Manager(config);
  manager.saveToken(app, createToken({ device: '123' }, 1));
  return new Promise((resolve, reject) => {
    Promise.delay(1500).then(reject);
    manager.tokenExpired.addOnce((token, application) => {
      t.is(application.client_id, app.client_id);
      t.is(manager.isTokenValid(token), false);
      resolve();
    });
  });
});
