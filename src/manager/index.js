import jwtDecode from 'jwt-decode';
import signals from 'signals';
import LocalStorage from '../storage/local';
import MemoryStorage from '../storage/memory';
import Watcher from './watcher';

export default class AccessManager {
  constructor(config, options) {
    this._config = config || {};

    if (!this._config.application) {
      throw new Error('Application is not configured');
    }

    this._application = this._config.application;
    this._applications = this._config.application ? [ this._config.application ] : [];

    this._stores = {};
    this._watchers = {};

    this.tokenExpired = new signals.Signal();
  }

  initialize() {
    return new Promise((resolve, reject) => {
      try {
        this._applications.forEach(application => {
          let token = this.getToken(application);

          if (!token || !token.access_token) {
            return;
          }

          let expires_in = this._getTokenLifespan(token);

          if (expires_in <= 0) {
            return;
          }

          let watcher = this._getWatcher(application);
          watcher.watch(token, expires_in);
        });

        const token = this.getToken(this._application),
              metadata = token ? this.getTokenMetadata(token) : undefined;

        resolve(metadata);
      }
      catch (err) {
        return reject(err);
      }
    });
  }

  saveToken(application, token) {
    if (!token || !token.access_token) {
      return Promise.reject(new Error('Invalid token.'));
    }

    let store = this._getStore(application);
    store.write(token);

    let watcher = this._getWatcher(application);
    watcher.watch(token, token.expires_in);

    return Promise.resolve();
  }

  getToken(application) {
    let store = this._getStore(application);
    return store.read();
  }

  getTokenMetadata(token) {
    try {
      return jwtDecode(token.access_token || token);
    }
    catch(e) {
      return null;
    }
  }

  isTokenValid(token) {
    return this._getTokenLifespan(token) >= 0;
  }

  isAccessValid(application) {
    let watcher = this._getWatcher(application),
        token = this.getToken(application);

    return Boolean(token) && !watcher.hasExpired;
  }

  removeToken(application) {
    let store = this._getStore(application);
    store.clear();

    let watcher = this._getWatcher(application);
    watcher.unwatch();

    return Promise.resolve();
  }

  _getStore(application) {
    let name = this._getName(application);

    if (!this._stores[name]) {
      const storage = this._config.credentials ? this._config.credentials.storage : 'local';

      switch (storage) {
        case 'memory':
          this._stores[name] = new MemoryStorage(name);
          break;
        case 'local':
        default:
          this._stores[name] = new LocalStorage(name);
          break;
      }
    }

    return this._stores[name];
  }

  _getWatcher(application) {
    let name = this._getName(application);

    if (!this._watchers[name]) {
      let watcher = this._watchers[name] = new Watcher(application);
      watcher.tokenExpired.add(event => this._onTokenExpired(event));
    }

    return this._watchers[name];
  }

  _getName(application) {
    if (!application) {
      throw new Error('Application is not provided.');
    }

    if (!application.client_id && !application.security) {
      throw new Error('Application has no client_id.');
    }

    return `api_access_${application.client_id || application.security}`;
  }

  _getTokenLifespan(token) {
    if (!token) {
      return 0;
    }

    const now = Math.round(new Date().getTime() / 1000),
          metadata = this.getTokenMetadata(token);

    if (!metadata) {
      return 0;
    }

    let expires_in = 0;

    if (metadata.exp) {
      expires_in = metadata.exp - now;
    }
    else if (metadata.iat && token.expires_in) {
      expires_in = (metadata.iat + token.expires_in) - now;
    }

    if (expires_in < 0) {
      return 0;
    }

    return expires_in;
  }

  _onTokenExpired(event) {
    this.tokenExpired.dispatch(event.token, event.application);
  }
};
