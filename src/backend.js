import DtsApiClient from './client';
import DtsApiLogger from './logger';
import DtsApiManager from './manager';

import AWS from 'aws-sdk';

class Backend {
  constructor(config) {
    this._config = config || {};

    this._logger = new DtsApiLogger(this._config);

    this._config.application = { security: 'oauth' };

    if (!this._config.credentials) {
      throw new Error('Backend credentials are missing.');
    }

    this._config.credentials.storage = 'memory';

    if (!this._config.api) {
      if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging') {
        this._logger.debug('Staging API client is starting...');
        this._config.api = { path: '/backend-v4/' };
      }
      else {
        this._logger.debug('Production API client is starting...');
        this._config.api = { path: '/backend-v3/' };
      }
    }

    if (process.env.BACKEND_APPLICATION) {
      this._application = JSON.parse(process.env.BACKEND_APPLICATION);
    }

    this._config.legacy = false;
    this._config.socket = false;

    const options = {
      proxy: () => this._ensureApiAccess(),
      logger: this._logger
    };

    this._client = new DtsApiClient(this._config, options);

    this._manager = new DtsApiManager(this._config, options);
    this._manager.tokenExpired.add(() => this._refreshAccess().catch(() => {
      this._logger.error('Unable to refresh the API token: ' + (!err || err.message || err.stack));
    }));

    this._ensureApiAccess().catch(err => this._logger.error(err));
  }

  get client() {
    return this._client;
  }

  _ensureApiAccess() {
    if (this._ensurePromise) {
      return this._ensurePromise;
    }

    this._ensurePromise = new Promise((res, rej) => {
      const resolve = () => {
        this._ensurePromise = null;
        res();
      };

      const reject = (e) => {
        this._ensurePromise = null;
        rej(e);
      };

      if (!this._application) {
        return this._refreshAccess().then(resolve).catch(reject);
      }

      const token = this._manager.getToken(this._application);

      if (!token || !this._manager.isTokenValid(token)) {
        return this._refreshAccess().then(resolve).catch(reject);
      }

      resolve();
    });

    return this._ensurePromise;
  }

  _refreshAccess() {
    let appPromise = Promise.resolve();

    if (!this._application) {
      this._logger.debug('Reading the API application details...');

      if (this._config.credentials.secret) {
        let secrets = new AWS.SecretsManager({ region: this._config.credentials.region });

        appPromise = secrets.getSecretValue({
          SecretId: this._config.credentials.secret
        }).promise()
        .then(response => {
          this._logger.debug('API application details received.');
          this._application = JSON.parse(response.SecretString);

          if (!this._application.security) {
            this._application.security = 'oauth';
          }
        })
        .catch(err => {
          this._logger.warn('Unable to load the API application details: ' + (!err || err.message || err.stack));
          return Promise.reject(err);
        });
      }
      else if (this._config.credentials.bucket) {
        let s3 = new AWS.S3({ region: this._config.credentials.region, signatureVersion: 'v4' });

        appPromise = s3.getObject({
          Bucket: this._config.credentials.bucket,
          Key: this._config.credentials.role + '/dts-backend-client.json'
        }).promise()
        .then(response => {
          this._logger.debug('API application details received.');
          this._application = JSON.parse(response.Body.toString('utf8'));

          if (!this._application.security) {
            this._application.security = 'oauth';
          }
        })
        .catch(err => {
          this._logger.warn('Unable to load the API application details: ' + (!err || err.message || err.stack));
          return Promise.reject(err);
        });
      }
      else {
        appPromise = Promise.reject(new Error('Missing credentials configuration.'));
      }
    }

    if (!this._token || !this._token.refresh_token) {
      return appPromise.then(() => {
        this._logger.debug('Authenticating the API application...');

        return this.client.api.request('oauth2', 'grantToken', {
          grant_type: 'client_credentials',
          client_id: this._application.client_id,
          client_secret: this._application.client_secret
        }, true)
        .then(token => {
          this._logger.debug('API application has been authenticated.');
          this._token = token;
          this.client.authorize(this._application, this._token);
          this._manager.saveToken(this._application, this._token);
        })
        .catch(err => {
          this._logger.error('Unable to authenticate the API application: ' + (!err || err.message || err.stack));
          return Promise.reject(err);
        });
      });
    }

    return appPromise.then(() => {
      this._logger.debug('Refreshing the API access...');

      return this.client.api.request('oauth2', 'grantToken', {
        grant_type: 'refresh_token',
        client_id: this._application.client_id,
        refresh_token: this._token.refresh_token
      }, true)
      .then(token => {
        this._logger.debug('API access has been refreshed.');
        this._token = token;
        this.client.authorize(this._application, this._token);
        this._manager.saveToken(this._application, this._token);
      })
      .catch(err => {
        this._logger.error('Unable to refresh the API access: ' + (!err || err.message || err.stack));
        return Promise.reject(err);
      });
    });
  }
}

export default Backend;
