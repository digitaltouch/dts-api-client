import AMA from 'aws-mobile-analytics-sdk';

class Analytics {
  constructor(config) {
    this._config = config;
  }

  initialize() {
    this._analytics = new AMA.Manager({
      clientOptions: { region: 'us-east-1' },
      appId: this._config.analytics.appId,
      appVersionName: this._config.version || '???',
      appVersionCode: this._config.version || '???',
      platform: this._config.analytics.platform || 'Windows'
    });
  }

  authenticate(user) {
  }
}

export default Analytics;
