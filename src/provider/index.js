import { config as Config, CognitoIdentityCredentials } from 'aws-sdk';
import { AuthenticationDetails, CognitoUser, CognitoUserAttribute, CognitoUserPool } from 'amazon-cognito-identity-js';

import jwtDecode from 'jwt-decode';
import signals from 'signals';
import uuid from 'uuid/v4';

import LocalStorage from '../storage/local';
import MemoryStorage from '../storage/memory';

const IDENTITY_STORE = 'api_identity';
const IDENTITY_BACKUP_STORE = 'api_identity_backup';

export default class AccessProvider {
  constructor(config, options) {
    this._config = config || {};
    this._logger = options.logger;

    this._stores = {};

    this.userChanged = new signals.Signal();
    this.userDetailsRequired = new signals.Signal();

    if (!this._config.cognito) {
      throw new Error('Cognito is not configured');
    }

    Config.update({
      credentials: new CognitoIdentityCredentials({
        IdentityPoolId: this._config.cognito.identityPoolId,
        Logins: {}
      }, { region: this._config.cognito.region }),
      region: this._config.cognito.region
    });

    this._cognito = new CognitoUserPool({
      ClientId: this._config.cognito.clientId,
      UserPoolId: this._config.cognito.userPoolId
    });

    this._identity = this._getStore('api_identity').read();

    this._cognito.getCurrentUser();
  }

  get user() {
    if (!this._user) {
      return null;
    }

    const user = {};

    const attributes = (this._user.UserAttributes || this._user.$TokenAttributes || []).reduce((result, attr) => {
      result[attr.Name] = attr.Value;
      return result;
    }, {});

    Object.keys(attributes).forEach(x => {
      let y = x;

      switch (x) {
        case 'name':
          y = 'first_name';
          break;
        case 'family_name':
          y = 'last_name';
          break;
        case 'preferred_username':
          y = 'username';
          break;
        case 'phone_number':
          y = 'phone';
          break;
        case 'phone_number_verified':
          y = 'phone_verified';
          break;
      }

      if (x.indexOf('custom:') === 0) {
        y = x.substring('custom:'.length);
      }

      user[y] = attributes[x];

      if (y === 'phone_verified') {
        user[y] = user[y] === 'true';
      }
    });

    if (user.phone && user.phone_verified === undefined) {
      user.phone_verified = true;
    }

    return user;
  }

  get identity() {
    return this._identity;
  }

  initialize() {
    return new Promise((resolve, reject) => {
      const user = this._cognito.getCurrentUser();

      if (!user) {
        return resolve(null);
      }

      user.getSession((err, session) => {
        if (err) {
          return reject(err);
        }

        this._applySession(session, user, true)
        .then(resolve)
        .catch(reject);
      });
    });
  }

  assumeIdentity(obj) {
    obj = obj || {};

    const store = this._getStore(IDENTITY_STORE);

    this._identity = store.read();

    if (this._identity && this._identity.token) {
      return Promise.resolve(this._identity);
    }

    const existing = Boolean(obj.token) && Boolean(obj.password);

    const token = obj.token || this._uuid(),
          password = obj.password || this._password(),
          code = this._code();

    const identity = {
      token: token,
      code: code,
      password: password
    };

    const request = {
      token: identity.token,
      username: identity.token,
      given_name: identity.code,
      password: identity.password,
      application: obj.application,
      platform: obj.platform
    };

    return this.signOut()
    .then(() => {
      if (!existing) {
        return this.signUp(request)
        .then(result => {
          identity.pending = !result.confirmed;
        });
      }
    })
    .then(() => {
      store.write(identity);
      this._identity = identity;
      return identity;
    });
  }

  resetIdentity() {
    const store = this._getStore(IDENTITY_STORE);

    const identity = store.read();

    if (identity) {
      const backupStore = this._getStore(IDENTITY_BACKUP_STORE);

      if (!backupStore.read()) {
        backupStore.write(identity);
      }
    }

    store.clear();
    this._identity = null;

    return this.signOut();
  }

  signUp(request) {
    return new Promise((resolve, reject) => {
      request = Object.assign({}, request);

      if (!request.token) {
        request.token = this._uuid();
      }

      const password = request.password || this._password();

      const attributeList = [];

      this._guestUser = null;

      Object.keys(request).forEach(x => {
        let y = x;

        switch (x) {
          case 'phone':
            y = 'phone_number';
            break;
          case 'username':
            y = 'preferred_username';
            break;
          case 'first_name':
            y = 'name';
            break;
          case 'last_name':
            y = 'family_name';
            break;
          case 'application':
          case 'business':
          case 'platform':
          case 'location':
          case 'token':
          case 'seat':
            y = 'custom:' + x;
            break;
          case 'password':
            return;
          default:
            break;
        }

        if (request[x] === undefined) {
          return;
        }

        attributeList.push(new CognitoUserAttribute({
          Name: y,
          Value: request[x]
        }));
      });

      let username;

      if (request.username) {
        username = request.username;
      }
      else if (request.phone) {
        username = request.phone;
      }
      else if (request.email) {
        username = request.email;
      }

      this._cognito.signUp(username, password, attributeList, null, (err, result) => {
        if (err) {
          return reject(err);
        }

        this._guestUser = result.user;

        const status = {
          confirmed: Boolean(result.userConfirmed)
        };

        resolve(status);
      });
    });
  }

  confirmSignUp(code) {
    return new Promise((resolve, reject) => {
      if (!this._guestUser) {
        return reject(new Error('No active user.'));
      }

      this._guestUser.confirmRegistration(code, true, (err, result) => {
        if (err) {
          return reject(err);
        }

        resolve(result === 'SUCCESS');
      });
    });
  }

  resendSignUpConfirmation(username) {
    return new Promise((resolve, reject) => {
      this._cognito.client.resendConfirmationCode({
        ClientId: this._config.cognito.clientId,
        Username: username
      }, (err, result) => {
        if (err) {
          return reject(err);
        }

        if (!this._guestUser) {
          this._guestUser = new CognitoUser({
            Username: username,
            Pool: this._cognito
          });
        }

        resolve();
      });
    });
  }

  confirmAttribute(name, code) {
    return new Promise((resolve, reject) => {
      if (!this._user) {
        return reject(new Error('Missing user.'));
      }

      switch (name) {
        case 'phone':
          name = 'phone_number';
          break;
        case 'email':
          break;
        default:
          return reject(new Error('Invalid attribute: ' + name));
      }

      this._user.verifyAttribute(name, code, {
        onSuccess: result => resolve(),
        onFailure: err => reject(err)
      });
    });
  }

  resendAttributeConfirmation(name) {
    return new Promise((resolve, reject) => {
      if (!this._user) {
        return reject(new Error('Missing user.'));
      }

      switch (name) {
        case 'phone':
          name = 'phone_number';
          break;
        case 'email':
          break;
        default:
          return reject(new Error('Invalid attribute: ' + name));
      }

      this._user.getAttributeVerificationCode(name, {
        onFailure: err => reject(err),
        inputVerificationCode() {
          resolve();
        }
      });
    });
  }

  signIn(request) {
    return new Promise((resolve, reject) => {
      if (!request) {
        if (!this.identity) {
          return reject(new Error('Missing sign in identity.'));
        }

        request = {
          username: this.identity.token,
          password: this.identity.password
        };
      }

      const authenticationDetails = new AuthenticationDetails({
        Username: request.username,
        Password: request.password
      });

      if (this._guestUser && this._guestUser.userName !== request.username) {
        try {
          this._guestUser.signOut();
        }
        catch (err) {}

        this._guestUser = null;
      }

      if (!this._guestUser) {
        this._guestUser = new CognitoUser({
          Username: request.username,
          Pool: this._cognito
        });
      }

      const details = {
        onSuccess: session => {
          const identity = this.identity;
          if (identity && identity.pending) {
            delete identity.pending;

            this._getStore(IDENTITY_STORE).write(identity);
          }

          this._applySession(session, this._guestUser)
          .then(() => {
            this._guestUser = null;
          })
          .then(resolve)
          .catch(reject);
        },
        onFailure: reject,
        newPasswordRequired: (userAttributes, requiredAttributes) => {
          const request = {
            userAttributes: userAttributes,
            requiredAttributes: requiredAttributes,
            done: (password) => {
              if (!password) {
                return reject(new Error('Sign in was cancelled.'));
              }

              this._guestUser.completeNewPasswordChallenge(password, {}, details);
            }
          };

          this.userDetailsRequired.dispatch(request);
        }
      };

      this._guestUser.authenticateUser(authenticationDetails, details);
    });
  }

  updateAttributes(attributes) {
    attributes = attributes || {};

    return new Promise((resolve, reject) => {
      if (!this._user) {
        return reject(new Error('Missing user.'));
      }

      const attributeList = Object.keys(attributes)
      .map(x => {
        let y = x,
            value = attributes[x];

        switch (x) {
          case 'phone':
            y = 'phone_number';
            break;
          case 'username':
            y = 'preferred_username';
            break;
          case 'first_name':
            y = 'name';
            break;
          case 'last_name':
            y = 'family_name';
            break;
          case 'application':
          case 'business':
          case 'platform':
          case 'location':
          case 'token':
          case 'seat':
          case 'password':
            return;
          default:
            break;
        }

        if (value === undefined) {
          return;
        }

        return new CognitoUserAttribute({
          Name: y,
          Value: value
        });
      }).filter(x => x);

      if (attributeList.length === 0) {
        return resolve(this.user);
      }

      this._user.updateAttributes(attributeList, (err, data) => {
        if (err) {
          return reject(err);
        }

        this._user.getUserAttributes((err, result) => {
          if (result) {
            this._user.UserAttributes = result;
          }

          this.userChanged.dispatch(this.user);

          resolve(this.user);
        });
      });
    });
  }

  getUserToken(current) {
    return new Promise((resolve, reject) => {
      const user = this._user || this._cognito.getCurrentUser();

      if (!user) {
        return resolve(null);
      }

      user.getSession((err, session) => {
        if (err) {
          return reject(err);
        }

        if (current) {
          return resolve(session.getAccessToken().getJwtToken());
        }
        else {
          const token = session.getRefreshToken();

          if (!token) {
            return reject(new Error('No refresh token.'))
          }

          user.refreshSession(token, (err, session) => {
            if (err) {
              return reject(err);
            }

            this._applySession(session, user)
            .then(() => resolve(session.getAccessToken().getJwtToken()))
            .catch(reject);
          });
        }
      });
    });
  }

  requestPasswordReset(username) {
    return new Promise((resolve, reject) => {
      this._cognito.client.forgotPassword({
        ClientId: this._config.cognito.clientId,
        Username: username
      }, (err, result) => {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    });
  }

  confirmPasswordReset(code, username, password) {
    return new Promise((resolve, reject) => {
      this._cognito.client.confirmForgotPassword({
        ClientId: this._config.cognito.clientId,
        ConfirmationCode: code,
        Password: password,
        Username: username
      }, (err, result) => {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    });
  }

  changePassword(oldPassword, newPassword) {
    return new Promise((resolve, reject) => {
      if (!this._user) {
        return reject(new Error('No active user.'));
      }

      this._user.getSession((err, session) => {
        if (err) {
          return reject(err);
        }

        this._cognito.client.changePassword({
          PreviousPassword: oldPassword,
          ProposedPassword: newPassword,
          AccessToken: session.getAccessToken().getJwtToken()
        }, (err, result) => {
          if (err) {
            return reject(err);
          }

          resolve();
        });
      });
    });
  }

  signOut() {
    return new Promise((resolve, reject) => {
      const user = this._cognito.getCurrentUser();

      if (user) {
        user.signOut();
      }

      if (!this._user) {
        return resolve();
      }

      try {
        this._user.signOut();
      }
      catch (err) {
        return reject(err);
      }

      this._guestUser = null;
      this._cognitosync = null;

      this._applySession()
      .catch(e => {})
      .then(resolve);
    });
  }

  _applySession(session, user, applyOnly) {
    return new Promise((resolve, reject) => {
      try {
        const ID = `cognito-idp.${this._config.cognito.region}.amazonaws.com/${this._config.cognito.userPoolId}`;

        let value;

        if (session && session.isValid()) {
          value = session.getIdToken().getJwtToken();
        }
        else {
          value = undefined;
        }

        if (Config.credentials.params.Logins[ID] === value) {
          return resolve();
        }

        applyOnly = applyOnly || Boolean(Config.credentials.params.Logins[ID]);

        Config.credentials.params.Logins[ID] = value;

        if (user) {
          if (session) {
            const token = session.getIdToken().getJwtToken(),
                  jwt = jwtDecode(token);

            user.$TokenAttributes = Object.keys(jwt).map(x => {
              return {
                Name: x,
                Value: jwt[x]
              };
            });
          }

          this._user = user;

          if (applyOnly) {
            resolve();
          }
          else {
            user.getUserAttributes((err, result) => {
              if (result) {
                user.UserAttributes = result;
              }

              this.userChanged.dispatch(this.user);

              resolve();
            });
          }
        }
        else {
          this._user = null;
          this.userChanged.dispatch(this.user);

          resolve();
        }
      }
      catch (err) {
        return reject(err);
      }
    });
  }

  _getStore(name) {
    if (!this._stores[name]) {
      const storage = this._config.credentials ? this._config.credentials.storage : 'local';

      switch (storage) {
        case 'memory':
          this._stores[name] = new MemoryStorage(name);
          break;
        case 'local':
        default:
          this._stores[name] = new LocalStorage(name);
          break;
      }
    }

    return this._stores[name];
  }

  _uuid() {
    return uuid();
  }

  _code() {
    const possible = '23456789ABCDEFGHKLMNPQRTUVWXYZ',
          length = 8;

    let text = '';

    for (let i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

  _password() {
    const possible = ['ABDFGHKMNQRTY','abdefghmnpqrty','23456789'];
    let text = '';

    for (let j = 0; j < possible.length; j++) {
      for (let i = 0; i < 5; i++) {
        text += possible[j].charAt(Math.floor(Math.random() * possible[j].length));
      }
    }

    return text;
  }
};
