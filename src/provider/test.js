import test from 'ava';

import Promise from 'bluebird';
import Provider from './index';

const app = { security: 'oauth', client_id: 'test' },
      config = {
        application: app,
        cognito: {
          region: 'us-east-1',
          clientId: '413u09m4adnd2it13735h9iflr',
          identityPoolId: 'us-east-1:7b7d90d5-f5de-4b18-9876-f9f8691eab36',
          userPoolId: 'us-east-1_c1hMdmHEr'
        },
        storageType: 'memory'
      };

test('provider: valid constructor', t => {
  try {
  const provider = new Provider(config);
  t.pass();
}catch(err) {console.error(err)}
});
