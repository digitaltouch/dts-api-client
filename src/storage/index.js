import LocalStorage from './local';
import MemoryStorage from './memory';

var stores = {};

export default {
  getStore: (config, name) => {
    if (!stores[name]) {
      const storage = config.storage || 'local';

      switch (storage) {
        case 'memory':
          stores[name] = new MemoryStorage(name);
          break;
        case 'local':
        default:
          stores[name] = new LocalStorage(name);
          break;
      }
    }

    return stores[name];
  }
};
