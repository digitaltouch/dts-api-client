export default class Storage {
  constructor(id) {
    this._id = id;
  }

  clear() {
    this._value = undefined;
  }

  read() {
    return this._value;
  }

  write(value) {
    this._value = value;
  }
};
