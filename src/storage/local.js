import store from 'store2';

export default class Storage {
  constructor(id) {
    this._id = id;
  }

  clear() {
    store.remove(this._id);
  }

  read() {
    return store.get(this._id);
  }

  write(value) {
    if (value) {
      store.set(this._id, value);
    }
    else {
      store.remove(this._id);
    }
  }
};
