import AWS from 'aws-sdk';
import Dataset from './dataset';

require('amazon-cognito-js');

class Profile {
  constructor(config, options) {
    this._config = config;
    this._logger = options.logger;

    this._datasets = {};
  }

  initialize() {
    this._sync = new AWS.CognitoSync({ region: this._config.cognito.region });
    this._manager = new AWS.CognitoSyncManager({ region: this._config.cognito.region });
  }

  authenticate(user) {
    Object.values(this._datasets).forEach(dataset => dataset.dispose());
    this._datasets = {};
  }

  getDataset(name) {
    if (!this._manager) {
      return Promise.reject(this._logger.error('Datasets are not available.'));
    }

    return new Promise((resolve, reject) => {
      if (this._datasets[name]) {
        return resolve(this._datasets[name]);
      }

      this._manager.openOrCreateDataset(name, (err, dataset) => {
        if (err) {
          return reject(this._logger.warn({ error: err, dataset: name }, 'Unable to open a dataset.'));
        }

        this._datasets[name] = new Dataset(dataset, this._logger);

        resolve(this._datasets[name]);
      });
    });
  }

  registerDevice(token) {
    if (!this._sync) {
      return Promise.reject(this._logger.error('No active session.'));
    }

    if (!this._config.cognito.pushNotifications) {
      return Promise.reject(this._logger.error('Device registration is not configured.'));
    }

    return new Promise((resolve, reject) => {
      this._getIdentityId()
      .then(identityId => {
        return this._sync.registerDevice({
          IdentityPoolId: this._config.cognito.identityPoolId,
          Platform: this._config.cognito.pushNotifications,
          IdentityId: identityId,
          Token: token
        }).promise();
      })
      .then(data => {
        this._deviceId = data.DeviceId;
        this._logger.debug({ device_id: data.DeviceId }, 'Device ID received.');
        resolve();
      })
      .catch(err => {
        reject(this._logger.warn({ error: err }, 'Unable to register a device.'));
      });
    });
  }

  _getIdentityId() {
    return new Promise((resolve, reject) => {
      AWS.config.credentials.get((err) => {
        if (err) {
          return reject(err);
        }

        resolve(AWS.config.credentials.identityId);
      });
    });
  }
}

export default Profile;
