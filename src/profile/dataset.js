export default class Dataset {
  constructor(dataset, logger) {
    this._dataset = dataset;
    this._logger = logger;
  }

  dispose() {
  }

  keys() {
    return new Promise((resolve, reject) => {
      this._dataset.getAll((err, value) => {
        if (err) {
          return reject(this._logger.warn({ error: err, dataset: this._dataset.datasetName }, 'Unable to fetch the dataset.'));
        }

        resolve(Object.keys(value));
      });
    });
  }

  read(key) {
    return new Promise((resolve, reject) => {
      this._dataset.get(key, (err, value) => {
        if (err) {
          return reject(this._logger.warn({ error: err, key: key, dataset: this._dataset.datasetName }, 'Unable to read from the dataset.'));
        }

        value = value ? JSON.parse(value) : null;

        resolve(value);
      });
    });
  }

  write(key, value) {
    return new Promise((resolve, reject) => {
      value = JSON.stringify(value);

      this._dataset.put(key, value, (err, record) => {
        if (err) {
          return reject(this._logger.warn({ error: err, key: key, dataset: this._dataset.datasetName }, 'Unable to write to the dataset.'));
        }

        resolve(record);
      });
    });
  }

  remove(key) {
    return new Promise((resolve, reject) => {
      this._dataset.remove(key, (err, record) => {
        if (err) {
          return reject(this._logger.warn({ error: err, key: key, dataset: this._dataset.datasetName }, 'Unable to remove from the dataset.'));
        }

        resolve(record);
      });
    });
  }

  sync() {
    return new Promise((resolve, reject) => {
      this._dataset.synchronize({
        onSuccess: (dataset, newRecords) => {
          this._logger.debug({ dataset: this._dataset.datasetName }, 'Dataset saved.');
          resolve();
        },
        onFailure: (err) => {
          this._logger.warn({ dataset: this._dataset.datasetName, error: err }, 'Dataset save error.');
          reject(err);
        },
        onConflict: (dataset, conflicts, callback) => {
          this._logger.debug({ dataset: this._dataset.datasetName }, 'Dataset conflict.');

          const resolved = [];

          for (let i = 0; i < conflicts.length; i++) {
            resolved.push(conflicts[i].resolveWithLocalRecord());
          }

          this._dataset.resolve(resolved, () => {
            resolve();
            return callback(true);
          });
        },
        onDatasetDeleted: (dataset, datasetName, callback) => {
          this._logger.debug({ dataset: this._dataset.datasetName }, 'Dataset deleted.');
          return callback(true);
        },
        onDatasetsMerged: (dataset, datasetNames, callback) => {
          this._logger.debug({ dataset: this._dataset.datasetName }, 'Dataset merged.');
          return callback(true);
        }
      });
    });
  }
};
