import { CloudWatch, CloudWatchLogs } from 'aws-sdk';

import Storage from '../../storage';

const CACHE_DELAY = 500;
const CACHE_LIMIT = 200;
const UPLOAD_DELAY = 10000;

export default class CloudWatchLogger {
  constructor(config) {
    this._config = config.cloudwatch;

    if (!this._config.group) {
      throw new Error('CloudWatch group is not configured.');
    }

    this._store = Storage.getStore(config, `api_logs_${this._config.group}`);
    this._tokenStore = Storage.getStore(config, `api_logs_token_${this._config.group}`);
    this._levelStore = Storage.getStore(config, `api_logs_level_${this._config.group}`);

    if (!this._config) {
      throw new Error('Missing CloudWatch config.');
    }

    this._queue = this._store.read() || [];
    this._sequenceToken = this._tokenStore.read();
    this._level = this._levelStore.read() || 30;
  }

  initialize() {
    // this._logger = new CloudWatchLogs({ region: this._config.region });
    // this._cw = new CloudWatch({ region: this._config.region });
  }

  get _logger() {
    return this.__logger || (this.__logger = new CloudWatchLogs({ region: this._config.region }));
  }

  get _cw() {
    return this.__cw || (this.__cw = new CloudWatch({ region: this._config.region }));
  }

  authenticate(user) {
    this._streamName = user ? user.token : null;
    this._location = user ? user.location : null;
  }

  setLevel(level) {
    this._level = level;
    this._levelStore.write(level);
  }

  log(data) {
    if (data.level && data.level < this._level) {
      return;
    }

    const payload = {
      message: JSON.stringify(data),
      timestamp: new Date(data.time).getTime()
    };

    this._queue.push(payload);

    this._scheduleCache();
    this._scheduleUpload();
  }

  sendMetrics(metrics) {
    const namespaces = metrics.reduce((res, metric) => {
      let ns = res[metric.namespace];

      if (!ns) {
        ns = res[metric.namespace] = [[]];
      }

      let group = ns[ns.length - 1];

      if (group.length >= 100) {
        group = [];
        ns.push(group);
      }

      group.push(metric);

      return res;
    }, {});

    const tasks = Object.keys(namespaces)
      .reduce((task, namespace) => {
        const groups = namespaces[namespace];

        return groups.reduce((task, group) => {
          return task.then(() => {
            return this._cw.putMetricData({
              Namespace: namespace,
              MetricData: group.map(metric => {
                let dimensions;

                if (metric.dimensions) {
                  dimensions = Object.keys(metric.dimensions)
                    .map(x => {
                      return {
                        Name: x,
                        Value: metric.dimensions[x]
                      };
                    });
                }

                return {
                  MetricName: metric.name,
                  Timestamp: metric.date,
                  Unit: metric.unit || 'Count',
                  Value: metric.value,
                  Dimensions: dimensions
                };
              })
            }).promise();
          });
        }, task);
      }, Promise.resolve());

    return tasks;
  }

  _scheduleCache() {
    if (!this._cacheTimer) {
      this._cacheTimer = setTimeout(() => {
        this._cacheTimer = null;

        if (this._queue.length > CACHE_LIMIT) {
          const offset = this._queue.length - CACHE_LIMIT;
          this._queue = this._queue.filter((x, i) => i >= offset);
        }

        this._store.write(this._queue);
      }, CACHE_DELAY);
    }
  }

  _scheduleUpload() {
    if (!this._uploadTimer) {
      this._uploadTimer = setTimeout(() => {
        this._uploadLogs()
        .catch(err => {})
        .then(() => {
          this._uploadTimer = null;

          this._scheduleCache();

          if (this._queue.length > 0) {
            this._scheduleUpload();
          }
        });
      }, UPLOAD_DELAY);
    }
  }

  _uploadLogs() {
    let time;

    const logs = [],
          later = [];

    this._queue.forEach(x => {
      if (!time) {
        time = x.timestamp
      }

      if (x.timestamp - time < 86400000) {
        logs.push(x);
      }
      else {
        later.push(x);
      }
    })

    this._queue = later;

    if (logs.length === 0 || !this._streamName) {
      return Promise.resolve();
    }

    return this._logger.putLogEvents({
      logEvents: logs,
      logGroupName: this._config.group,
      logStreamName: this._streamName,
      sequenceToken: this._sequenceToken
    }).promise()
    .then(data => {
      this._sequenceToken = data.nextSequenceToken;
      this._tokenStore.write(this._sequenceToken);
    })
    .catch(err => {
      this._queue = logs.concat(this._queue);

      if (err.code === 'DataAlreadyAcceptedException' || err.code === 'InvalidSequenceTokenException') {
        let value = err.message.split(': ')[1];
				this._sequenceToken = value !== 'null' ? value : undefined;
        this._tokenStore.write(this._sequenceToken);
			}
      else if (err.code === 'ResourceNotFoundException') {
        return this._logger.createLogStream({
          logGroupName: this._config.group,
          logStreamName: this._streamName
        }).promise();
      }
    });
  }
}
