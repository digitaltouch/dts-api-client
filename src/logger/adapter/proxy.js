export default class ProxyLogger {
  constructor(config) {
    this._config = config;
    this._logger = config.logger;

    if (!this._logger) {
      throw new Error('No logger provided.');
    }
  }

  initialize() {
  }

  authenticate(user) {
  }

  setLevel(level) {
    this._level = level;

    if (this._logger.setLevel) {
      this._logger.setLevel(level);
    }
  }

  sendMetrics(metrics) {
    return this._logger.sendMetrics(metrics);
  }

  log(data) {
    try {
      let level;

      switch (data.level) {
        case 10:
        case 20:
          level = 'debug';
          break;
        case 30:
          level = 'info';
          break;
        case 40:
          level = 'warn';
          break;
        case 50:
          level = 'error';
          break;
        case 60:
          level = 'fatal';
          break;
        default:
          level = 'debug';
          break;
      }

      this._logger[level].apply(this._logger, Object.values(data));
    }
    catch (err) {}
  }
}
