export default class ConsoleLogger {
  constructor(config) {
    this._config = config;
  }

  initialize() {
  }

  authenticate(user) {
  }

  setLevel(level) {
    this._level = level;
  }

  sendMetrics(metrics) {
    metrics.forEach(metric => console.log(`${metric.name}: ${metric.value} (${metric.unit || 'Count'})`));
    return Promise.resolve();
  }

  log(data) {
    try {
      let method;

      switch (data.level) {
        case 10:
        case 20:
          method = console.debug || console.log;
          break;
        case 30:
          method = console.log;
          break;
        case 40:
          method = console.warn;
          break;
        case 50:
        case 60:
          method = console.error;
          break;
        default:
          method = console.log;
          break;
      }

      method.apply(null, Object.values(data));
    }
    catch (err) {}
  }
}
