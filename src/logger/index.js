import ConsoleLogger from './adapter/console';
import CloudWatchLogger from './adapter/cloudwatch';
import ProxyLogger from './adapter/proxy';

class Logger {
  constructor(config) {
    this._config = config;

    this._level = 20;
    this._loggers = [];

    if (this._config.cloudwatch) {
      this._loggers.push(new CloudWatchLogger(config));
    }
    else if (this._config.logger) {
      this._loggers.push(new ProxyLogger(config));
    }
    else {
      this._loggers.push(new ConsoleLogger(config));
    }
  }

  initialize(identity) {
    this._loggers.forEach(logger => logger.initialize(identity));
  }

  authenticate(user) {
    this._loggers.forEach(logger => logger.authenticate(user));
  }

  setLevel(level) {
    this._level = level;
    this._loggers.forEach(logger => logger.setLevel(level));
  }

  debug(data, msg) {
    this._log(20, data, msg);
  }

  info(data, msg) {
    this._log(30, data, msg);
  }

  warn(data, msg) {
    this._log(40, data, msg);
    return data.error || new Error(msg || data);
  }

  error(data, msg) {
    this._log(50, data, msg);
    return data.error || new Error(msg || data);
  }

  fatal(data, msg) {
    this._log(60, data, msg);
    return data.error || new Error(msg || data);
  }

  sendMetrics(metrics) {
    return Promise.all(this._loggers.map(logger => logger.sendMetrics(metrics)));
  }

  _log(level, data, msg) {
    if (this._level > level) {
      return;
    }

    try {
      if (!data || typeof data !== 'object') {
        data = { message: data };
      }

      data.level = level;
      data.time = (new Date()).toISOString();

      if (msg) {
        data.message = msg;
      }

      if (this._config.version) {
        data.version = this._config.version;
      }

      if (level >= 40 && data.error) {
        let err = data.error;

        if (err.errObj) {
          err = err.errObj;
        }
        else if (err.obj) {
          err = err.obj;
        }

        data.error = {
          message: err.message || JSON.stringify(err),
          stack: err.stack
        };
      }

      this._loggers.forEach(logger => logger.log(data));
    }
    catch (err) {
      console.error(err);
    }
  }
}

export default Logger;
