import signals from 'signals';

module.exports = class Client {
  constructor() {
    this._sessionError = new signals.Signal();
  }

  get sessionError() {
    return this._sessionError;
  }

  authorize(application, token) {
  }

  deauthorize(application) {
  }
}
