'use strict';

const Client = require('../client');
const SwaggerClient = require('swagger-client');

class ApiError extends Error {
  constructor(obj) {
    super(typeof obj === 'string' ? obj : obj.message || 'API error.');

    Object.keys(obj).forEach(x => this[x] = obj[x]);

    this.name = 'ApiError';
  }
}

module.exports = class ApiClient extends Client {
  constructor(config) {
    super();

    if (!config.host && !config.spec) {
      throw new Error('Invalid configuration');
    }

    this._config = config;
    this._proxy = config.proxy;
    this._logger = config.logger;
  }

  //-----------------------------------------------
  //    Public methods
  //-----------------------------------------------

  authorize(application, token) {
    return this._getClientPromise(true).then(client => {
      client.clientAuthorizations.add(application.security, new SwaggerClient.ApiKeyAuthorization('Authorization', `Bearer ${token.access_token}`, 'header'));
    });
  }

  deauthorize(application) {
    return this._getClientPromise(true).then(client => {
      client.clientAuthorizations.remove(application.security);
    });
  }

  getAuthorizationUrl(application) {
    return this._getClientPromise(true).then(client => {
      let security = client.swaggerObject.securityDefinitions;

      if (!security[application.security] || !security[application.security].authorizationUrl) {
        this._logger.error(`Missing authorization info for security type '${application.security}'`);
        return Promise.reject(new Error(`Missing authorization info for security type '${application.security}'`));
      }

      return security[application.security].authorizationUrl +
        `?client_id=${encodeURIComponent(application.client_id)}&response_type=token` +
        `&redirect_uri=${encodeURIComponent(application.redirect_uri)}`;
    });
  }

  request(service, method, payload, skipAuthorization) {
    return this._getClientPromise(skipAuthorization).then(client => {
      if (!client[service]) {
        this._logger.error(`API service '${service}' not found.`);
        return Promise.reject(new Error(`API service '${service}' not found.`));
      }

      let implementation = client[service][method];

      if (!implementation) {
        this._logger.error(`API method '${method}' of service '${service}' not found.`);
        return Promise.reject(new Error(`API method '${method}' of service '${service}' not found.`));
      }

      return implementation(payload)
      .then(res => res.obj)
      .catch(err => {
        if (err) {
          if (err.obj) {
            return Promise.reject(new ApiError(err.obj));
          }

          if (err.status === 0 || err.statusCode === 0) {
            return Promise.reject(new ApiError({
              status: 0,
              message: 'Application is offline.'
            }));
          }

          if (err.status > 0 || err.statusCode > 0) {
            return Promise.reject(new ApiError({
              status: err.statusCode || err.status,
              url: err.url,
              message: err.message || err.statusText,
              method: err.method
            }));
          }

          if (err.errorObj) {
            return Promise.reject(new ApiError(err.errorObj));
          }

          return Promise.reject(err);
        }

        return Promise.reject(new Error('Unknown client error.'));
      });
    });
  }

  //-----------------------------------------------
  //    Private methods
  //-----------------------------------------------

  _getClientPromise(skipProxy) {
    if (!this._client) {
      let url, spec;

      if (this._config.spec) {
        spec = this._config.spec;
      }
      else if (this._config.host) {
        url = 'http' + (this._config.secure ? 's' : '') + '://';
        url += this._config.host + this._config.path + 'swagger.json';
      }

      this._client = new SwaggerClient({
        url: url,
        spec: spec,
        usePromise: true
      });
    }

    return this._client
      .then(client => client, err => {
        this._client = null;
        this._logger.error('Unable to get the API specification: ' + (!err || err.message || err.stack));
        return Promise.reject(err);
      })
      .then(client => !skipProxy ? this._proxy().then(() => client) : client);
  }
};
