import ApiClient from './api';
import CdnClient from './cdn';
import SocketClient from './socket';
import UploadClient from './upload';

import signals from 'signals';

export default class Client {
  constructor(config, options) {
    this._sessionError = new signals.Signal();

    const application = { security: 'oauth' };

    const proxy = options.proxy || (() => Promise.resolve()),
          awsproxy = options.awsproxy || (() => Promise.resolve()),
          logger = options.logger;

    const cfg = {
      application: application,
      api: {
        host: 'api.snap.menu',
        path: '/v3/',
        secure: true,
        application: application,
        proxy: proxy,
        logger: logger
      },
      cdn: {
        host: 'cdn.snap.menu',
        secure: false,
        path: '/',
        application: application,
        proxy: proxy,
        logger: logger
      },
      socket: {
        host: 'hub.snap.menu',
        secure: true,
        path: '/',
        application: application,
        proxy: proxy,
        logger: logger
      },
      upload: {
        bucket: 'snap-uploads',
        accelerated: true,
        region: 'us-west-2',
        application: application,
        proxy: awsproxy,
        logger: logger
      }
    };

    if (config) {
      this._config = Object
        .keys(config)
        .reduce((cfg, key) => {
          if (cfg.hasOwnProperty(key)) {
            if (config[key]) {
              cfg[key] = Object.assign(cfg[key], config[key]);
            }
            else if (config[key] === false) {
              cfg[key] = false;
            }
          }
          else {
            cfg[key] = config[key];
          }

          return cfg;
        }, cfg);
    }
    else {
      this._config = config;
    }

    this._clients = [];
    this._options = options || {};

    if (this._config.api) {
      this._clients.push(this.api = new ApiClient(this._config.api));
    }

    if (this._config.cdn) {
      this._clients.push(this.cdn = new CdnClient(this._config.cdn));
    }

    if (this._config.socket) {
      this._clients.push(this.socket = new SocketClient(this._config.socket));
    }

    if (this._config.upload) {
      this._clients.push(this.upload = new UploadClient(this._config.upload));
    }

    this._clients.forEach(client => {
      client.sessionError.add(event => this.sessionError.dispatch(event));
    });
  }

  //-----------------------------------------------
  //    Public methods
  //-----------------------------------------------

  get sessionError() {
    return this._sessionError;
  }

  authorize(application, token) {
    return Promise.all(this._clients.map(client => client.authorize(application, token) || Promise.resolve()));
  }

  deauthorize(application) {
    return Promise.all(this._clients.map(client => client.deauthorize(application) || Promise.resolve()));
  }
}
