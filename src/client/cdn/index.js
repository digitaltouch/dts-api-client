import Client from '../client';
import utils from '../utils';

class CdnClient extends Client {
  constructor(options) {
    super();

    this._options = options;
    this._url = utils.getHostUrl(this._options);
  }

  //-----------------------------------------------
  //    Public methods
  //-----------------------------------------------

  authorize(application, token) {
    return Promise.resolve();
  }

  deauthorize(application) {
    return Promise.resolve();
  }

  getUrl(key) {
    if (key.startsWith('/')) {
      key = key.substring(1);
    }

    return this._url + key;
  }

  load(key, onProgress) {
    return this._request(key, onProgress);
  }

  loadBinary(key, onProgress) {
    return this._request(key, 'blob', onProgress);
  }

  loadHeaders(key) {
    return new Promise((resolve, reject) => {
      const src = this._url + key;
      const xhr = new XMLHttpRequest();

      const onError = (event) => {
        reject(new Error('Unable to load ' + src));
      }

      xhr.addEventListener('load', e => {
        if ((xhr.status === 200 || xhr.status === 304) && xhr.readyState === 4) {
          return resolve(xhr.getAllResponseHeaders());
        }

        onError(e);
      }, false);
      xhr.addEventListener('error', onError, false);

      xhr.open('HEAD', src, true);
      xhr.send();
    });
  }

  //-----------------------------------------------
  //    Private methods
  //-----------------------------------------------

  _request(key, responseType, onProgress) {
    onProgress = onProgress || (() => {});
    const src = this._url + key;

    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      if (responseType) {
        xhr.responseType = responseType;
      }

      const onError = (event) => {
        reject(new Error('Unable to load ' + src));
      }

      xhr.addEventListener('load', e => {
        if ((xhr.status === 200 || xhr.status === 304) && xhr.readyState === 4) {
          return resolve(xhr.response);
        }

        onError(e);
      }, false);
      xhr.addEventListener('error', onError, false);
      xhr.addEventListener('progress', onProgress, false);

      xhr.open('GET', src, true);
      xhr.send();
    });
  }
};

export default CdnClient;
