import { config as Config, S3 } from 'aws-sdk';
import uuid from 'uuid/v4';

import Client from '../client';

class UploadClient extends Client {
  constructor(options) {
    super();

    this._options = options;
    this._proxy = options.proxy;
  }

  //-----------------------------------------------
  //    Public methods
  //-----------------------------------------------

  put(request, file, onProgress) {
    onProgress = onProgress || (() => {});

    return this._proxy()
    .then(() => {
      const bucket = request.bucket || this._options;
      const S3Service = this._s3 || (this._s3 = new S3({
        region: bucket.region,
        useAccelerateEndpoint: Boolean(bucket.accelerated)
      }));
      const key = `${bucket.path || ''}${request.key || uuid()}`;

      return new Promise((resolve, reject) => {
        function upload(path) {
          S3Service.upload({
            Bucket: bucket.bucket,
            Body: file,
            Key: path,
            ContentType: request.mime || file.type || 'application/octet-stream'
          }, (err, data) => {
            if (err) {
              return reject(err);
            }

            resolve(path);
          })
          .on('httpUploadProgress', event => onProgress({
            loaded: event.loaded,
            total: event.total
          }));
        }

        if (!request.bucket) {
          Config.credentials.get((err) => {
            if (err) {
              return reject(err);
            }

            const path = `${Config.credentials.identityId}/${key}`;
            upload(path);
          });
        }
        else {
          Config.credentials.get((err) => {
            if (err) {
              return reject(err);
            }

            upload(key);
          });
        }
      });
    })
    .catch(err => {
      if (err.code) {
        this.sessionError.dispatch(err);
      }

      return Promise.reject(err);
    });
  }
}

export default UploadClient;
