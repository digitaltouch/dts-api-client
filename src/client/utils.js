module.exports.getHostUrl = (host) => {
  let path = '';

  if (host.host) {
    if (host.protocol) {
      path += `${host.protocol}://`;
    }
    else if (host.secure) {
      path += `https://`;
    }
    else {
      if (typeof window !== 'undefined') {
        if (window.location.protocol === 'file:') {
          path += `http://`;
        }
        else if (window.location.protocol) {
          path += `${location.protocol}//`;
        }
        else {
          path += `//`;
        }
      }
      else {
        path += `https://`;
      }
    }

    path += host.host;

    if (host.port) {
      path += ':' + host.port;
    }
  }

  if (host.path) {
    if (host.host && !host.path.startsWith('/')) {
      path += '/';
    }

    path += host.path;
  }

  if (path.length > 0 && !path.endsWith('/')) {
    path += '/';
  }

  return path;
}
