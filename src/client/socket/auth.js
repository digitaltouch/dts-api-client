class AuthEngine {
  constructor() {
    this._storage = {};
    this.name = 'socket';
  }

  saveToken(name, token, options, callback) {
    this._storage[name] = token;
    callback && callback();
  }

  removeToken(name, callback) {
    delete this._storage[name];
    callback && callback();
  }

  loadToken(name, callback) {
    let token = this._storage[name] || null;
    callback(null, token);
  }
}

export default AuthEngine;
