import Client from '../client';
import AuthEngine from './auth';

import SC from 'socketcluster-client';
import signals from 'signals';
import uuid from 'uuid/v4';

class SocketClient extends Client {
  constructor(config) {
    super();

    this._options = config;
    this._proxy = config.proxy;

    this.isAuthenticatedChanged = new signals.Signal();
    this.isConnectedChanged = new signals.Signal();
    this.onError = new signals.Signal();

    this._channels = {};
    this._rpc = {};
    this._retrySubscriptions = [];
    this._isAuthenticated = undefined;
    this._isConnected = false;

    this._authEngine = new AuthEngine();

    this._onRpcMessage = new signals.Signal();

    this.onPingError = new signals.Signal();
  }

  //-----------------------------------------------
  //    Properties
  //-----------------------------------------------

  get isEnabled() {
    return this._isEnabled || false;
  }

  set isEnabled(value) {
    value = Boolean(value);

    if (this._isEnabled === value) {
      return;
    }

    this._isEnabled = value;

    if (value) {
      this._connect();
    }
  }

  get isAuthenticated() {
    return this._isAuthenticated;
  }

  set isAuthenticated(value) {
    value = Boolean(value);

    if (this._isAuthenticated === value) {
      return;
    }

    this._isAuthenticated = value;
    this.isAuthenticatedChanged.dispatch(value);
  }

  get isConnected() {
    return this._isConnected;
  }

  set isConnected(value) {
    value = Boolean(value);

    if (this._isConnected === value) {
      return;
    }

    this._isConnected = value;
    this.isConnectedChanged.dispatch(value);
  }

  //-----------------------------------------------
  //    Public methods
  //-----------------------------------------------

  authorize(application, token) {
    token = Object.assign({}, token);

    return new Promise((resolve, reject) => {
      if (this._options.application.security === application.security) {
        if (this.isConnected) {
          this._socket.authenticate(token.access_token, err => {
            if (err) {
              this.isAuthenticated = false;
              reject(err);
            }
            else {
              this.isAuthenticated = true;
              resolve();
            }
          });
        }
        else {
          this._authEngine.saveToken(this._authEngine.name, token.access_token);
          this.isAuthenticated = true;
          resolve();
        }
      }
      else {
        this.isAuthenticated = true;
        resolve();
      }
    });
  }

  deauthorize(application) {
    return new Promise(resolve => {
      if (this._options.application.security === application.security) {
        if (this.isConnected) {
          this._socket.removeAuthToken(() => {
            this.isAuthenticated = false;
            resolve();
          });
        }
        else {
          this._authEngine.removeToken(this._authEngine.name, () => {
            this.isAuthenticated = false;
            resolve();
          });
        }
      }
    });
  }

  subscribe(topic, handler) {
    this._getChannel(topic).watch(handler);
  }

  unsubscribe(topic, handler) {
    this._getChannel(topic).unwatch(handler);
  }

  publish(topic, data) {
    return this._proxy().then(() => {
      try {
        this._getChannel(topic).publish(data);
      }
      catch (err) {
        return Promise.reject(err);
      }
    });
  }

  on(name, handler) {
    this._socket.on(name, handler);
  }

  off(name, handler) {
    this._socket.off(name, handler);
  }

  emit(name, data) {
    return this._proxy().then(() => {
      return new Promise((resolve, reject) => {
        this._socket.emit(name, data, (err, data) => {
          if (err) {
            return reject(this._handleError(err));
          }

          resolve(data);
        });
      });
    });
  }

  request(topic, payload, timeout) {
    return this._proxy().then(() => {
      return new Promise((resolve, reject) => {
        timeout = timeout || 10000;

        if (!this.isEnabled) {
          return reject(new Error('Socket is not enabled.'));
        }

        if (!this._rpc[topic]) {
          this._rpc[topic] = (message) => {
            this._onRpcMessage.dispatch(topic, message);
          };

          this.subscribe(topic, this._rpc[topic]);
        }

        const timeoutID = setTimeout(() => {
          this._onRpcMessage.remove(handler);
          reject(new Error('Request has timed out.'));
        }, timeout);

        const request = {
          source: uuid(),
          payload: payload
        };

        const handler = (name, response) => {
          if (name !== topic || !response || response.destination !== request.source) {
            return;
          }

          this._onRpcMessage.remove(handler);
          clearTimeout(timeoutID);

          if (response.error) {

            reject(response.error);
          }
          else {
            resolve(response.payload);
          }
        }

        this._onRpcMessage.add(handler);

        this.publish(topic, request);
      });
    });
  }

  respond(topic, handler) {
    return new Promise((resolve, reject) => {
      if (!this.isEnabled) {
        return reject(new Error('Socket is not enabled.'));
      }

      if (!this._rpc[topic]) {
        this._rpc[topic] = true;
      }
      else {
        return reject(new Error('Handler already exists.'));
      }

      this.subscribe(topic, request => {
        if (!request.source) {
          return;
        }

        handler(request.payload, (error, payload) => {
          const response = {
            destination: request.source,
            payload: payload,
            error: error
          };

          this.publish(topic, response);
        });
      });

      resolve();
    });
  }

  ping(location) {
    //return this._proxy().then(() => {
    return Promise.resolve().then(() => {
      return new Promise((resolve, reject) => {
        const timeout = 10 * 1000;
        const timeoutID = setTimeout(() => {
          reject(new Error('Request has timed out.'));
        }, timeout);

        if (!this.isConnected) {
          reject(new Error('Not connected.'));
          return;
        }

        this.emit(`event:monitoring:${location}`, { type: 'ping' })
        .then(res => {
          clearTimeout(timeoutID);

          if (res.authorized === true || res.authorized === false) {
            this.isAuthenticated = res.authorized === true;

            if (!res.authorized) {
              this.onPingError.dispatch();
            }
          }

          resolve();
        })
        .catch(err => {
          if (err.message === 'Not authorized.') {
            this.onPingError.dispatch();
          }

          clearTimeout(timeoutID);
          reject(err);
        });
      });
    });
  }

  reconnect() {
    if (!this._socket) {
      return;
    }

    this._socket.disconnect();
    this._socket.connect();
  }

  //-----------------------------------------------
  //    Private methods
  //-----------------------------------------------

  _connect() {
    try {
      this._socket = SC.connect({
        hostname: this._options.host,
        path: this._options.path,
        port: this._options.port,
        secure: Boolean(this._options.secure),
        authEngine: this._authEngine,
        authTokenName: this._authEngine.name
      });

      this._socket.on('connect', status => {
        this.isConnected = true;
        this.isConnectedChanged.dispatch(this.isConnected);
      });
      this._socket.on('disconnect', () => {
        this.isConnected = false;
        this.isConnectedChanged.dispatch(this.isConnected);
      });
      this._socket.on('error', err => {
        this.onError.dispatch(this._handleError(err));
      });
      this._socket.on('subscribeFail', (err, channel) => {
        this.onError.dispatch(this._handleError(err));

        if (this._retrySubscriptions.indexOf(channel) === -1) {
          this._retrySubscriptions.push(channel);
        }
      });
      this._socket.on('authenticate', () => {
        this._retrySubscriptions.forEach(name => {
          const channel = this._getChannel(name);
          const watchers = channel.watchers();

          this._socket.unsubscribe(name);

          watchers.forEach(handler => {
            channel.subscribe(name, handler);
          });
        });
        this._retrySubscriptions = [];
      });
    }
    catch (err) {
      this._options.logger.error('Unable to create a WebSocket', err);
    }
  }

  _getChannel(topic) {
    return this._channels[topic] || (this._channels[topic] = this._socket.subscribe(topic));
  }

  _handleError(err) {
    if (err === 'Not authorized.' || err.message === 'Not authorized.') {
      this.isAuthenticated = false;
    }

    return typeof err === 'string' ? new Error(err) : err;
  }
}

export default SocketClient;
