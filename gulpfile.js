var gulp = require('gulp');
var concat = require('gulp-concat');
var bump = require('gulp-bump');
var git = require('gulp-git');
var del = require('del');
var runSequence = require('run-sequence');
var webpack = require('webpack-stream');
var fs = require('fs');
var pkg = require('./package.json');
var exec = require('child_process').exec;

gulp.task('clean', function() {
  return del(['./dist']);
});

gulp.task('webpack-frontend', function() {
  return gulp.src('src/')
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('dist/'));
});

gulp.task('webpack-backend', function() {
  return gulp.src('src/')
    .pipe(webpack(require('./webpack.backend.js')))
    .pipe(gulp.dest('dist/'));
});

gulp.task('tag', function(cb){
  return git
    .tag(pkg.version, pkg.version, function(err) {
      if (err) throw err;
      cb();
    });
});

gulp.task('add', function() {
  return gulp.src(['./*.*'])
    .pipe(git.add());
});

gulp.task('commit', function() {
  return gulp.src(['./*.*'])
    .pipe(git.commit('Build #' + pkg.version));
});

gulp.task('bump', function() {
  return gulp.src(['package.json'])
    .pipe(bump({ type: 'version' }))
    .pipe(gulp.dest('./'));
});

gulp.task('git', function(callback) {
  runSequence('add', 'tag', callback);
});

gulp.task('build', function(callback) {
  runSequence(['clean'], ['webpack-frontend', 'webpack-backend'], callback);
});

gulp.task('version', function(callback) {
  runSequence('git', 'bump', 'commit', callback);
});
