'use strict';

module.exports = require('./webpack.make.js')({
  platform: 'frontend',
  env: 'production',
  mode: 'release'
});
