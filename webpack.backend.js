'use strict';

module.exports = require('./webpack.make.js')({
  platform: 'backend',
  env: 'production',
  mode: 'release'
});
