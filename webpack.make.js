'use strict';

const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const pkg = require('./package.json');

module.exports = function (options) {
  const platform = options.platform;
  const env = options.env;
  const mode = options.mode;
  const version = pkg.version;

  const build = {
    platform: platform,
    env: env,
    mode: mode,
    version: version
  };

  console.log('platform:  ' + platform);
  console.log('env:       ' + env);
  console.log('mode:      ' + mode);
  console.log('version:   ' + version);

  const babel = Object.assign({}, pkg.babel);

  if (build.platform === 'backend') {
    babel.plugins = (babel.plugins || []).concat([
      'add-module-exports'
    ]);
  }

  const cfg = {
    module: {
      loaders: [
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: babel
        },
        {
          test: /\.json$/,
          loaders: ['json']
        }
      ]
    },
    resolveLoader: {
      root: path.join(__dirname, 'node_modules')
    },
    plugins: [
    ]
  };

  switch (build.platform) {
    case 'frontend':
      cfg.entry = {
        index: path.join(__dirname, 'src/frontend.js')
      };
      cfg.output = {
        path: path.resolve(__dirname, 'dist'),
        library: 'index',
        libraryTarget: 'commonjs2',
        filename: 'dts-api-client.min.js'
      };
      cfg.resolve = {
        alias: {
          'crypto-browserify': path.join(__dirname, './vendor/crypto-browserify'),
          'socketcluster-client': path.join(__dirname, './vendor/socketcluster-client/index.js'),
          'globals-shim': path.join(__dirname, './shims/globals.js')
        }
      };
      cfg.plugins.push(
        new webpack.DefinePlugin({
          'process.env': {
            NODE_ENV: JSON.stringify(build.mode === 'release' ? 'production' : 'development')
          }
        }),
        new webpack.ProvidePlugin({
          'global': 'globals-shim'
        })
      );
      break;
    case 'backend':
      cfg.target = 'node';
      cfg.node = {
        __filename: true,
        __dirname: true
      };
      cfg.entry = {
        index: path.join(__dirname, 'src/backend.js')
      };
      cfg.output = {
        path: path.resolve(__dirname, 'dist'),
        library: 'index',
        libraryTarget: 'commonjs2',
        filename: 'dts-backend-client.js'
      };
      cfg.externals = Object.keys(pkg.dependencies).concat(Object.keys(pkg.devDependencies));
      break;
  }

  switch (build.mode) {
    case 'debug':
      cfg.devtool = 'eval';
      break;

    case 'release':
      cfg.devtool = 'source-map';
      cfg.plugins.push(
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.DedupePlugin()
      );

      if (build.platform === 'frontend') {
        cfg.plugins.push(new webpack.optimize.UglifyJsPlugin({
          compressor: {
            screw_ie8: true,
            warnings: false
          }
        }));
      }
      break;
  }

  return cfg;
};
